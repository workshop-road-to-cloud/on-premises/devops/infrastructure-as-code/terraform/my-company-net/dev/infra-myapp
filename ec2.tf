resource "aws_instance" "frontend-ec2-01" {
    ami = "ami-06ca3ca175f37dd66"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.instance-frontend.id]
}

resource "aws_instance" "frontend-ec2-02" {
    ami = "ami-06ca3ca175f37dd66"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.instance-frontend.id]
}

resource "aws_instance" "backend-ec2-01" {
    ami = "ami-06ca3ca175f37dd66"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.instance-backend.id]
}

resource "aws_instance" "backend-ec2-02" {
    ami = "ami-06ca3ca175f37dd66"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.instance-backend.id]
}

resource "aws_instance" "backend-ec2-03" {
    ami = "ami-06ca3ca175f37dd66"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.instance-backend.id]
}