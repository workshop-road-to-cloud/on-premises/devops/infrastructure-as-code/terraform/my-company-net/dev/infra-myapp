provider "aws" {
  profile    = "default"
  region     = "us-east-1"
}

data "aws_vpc" "default" {
  default = true
}